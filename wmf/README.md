What to do when a patch-level release of buildkitd comes out:

File a Phabricator task with information from the release announcement,
including the list of notable changes.

```
dancy@base:~/src/wmf/buildkit$ git remote -v
origin	git@gitlab.wikimedia.org:repos/releng/buildkit.git (fetch)
origin	git@gitlab.wikimedia.org:repos/releng/buildkit.git (push)
upstream	https://github.com/moby/buildkit.git (fetch)
upstream	https://github.com/moby/buildkit.git (push)
```

```
dancy@base:~/src/wmf/buildkit$ git branch
  master
  wmf/v0.10
  wmf/v0.11
  wmf/v0.12
  wmf/v0.13
  wmf/v0.14
* wmf/v0.15
```

```
dancy@base:~/src/wmf/buildkit$ git pull --ff
Already up to date.
```

```
dancy@base:~/src/wmf/buildkit$ git fetch upstream --tags
remote: Enumerating objects: 496, done.        
remote: Counting objects: 100% (496/496), done.        
remote: Compressing objects: 100% (299/299), done.        
remote: Total 496 (delta 262), reused 370 (delta 189), pack-reused 0 (from 0)        
Receiving objects: 100% (496/496), 385.92 KiB | 2.66 MiB/s, done.
Resolving deltas: 100% (262/262), completed with 28 local objects.
From https://github.com/moby/buildkit
   bc92b63b9..49f3d8f82  master     -> upstream/master
   509c08c6c..9e14164a1  v0.15      -> upstream/v0.15
 * [new tag]             v0.15.2    -> v0.15.2
```

So we have new release v0.15.2 and we most recently handled v0.15.1.
Let's get a summary of what commits were added since then:

```
dancy@base:~/src/wmf/buildkit$ git log --oneline ..v0.15.2
9e14164a1 (tag: v0.15.2, upstream/v0.15) Merge pull request #5258 from tonistiigi/v0.15.2-picks
e24cd7c11 remotecache: handle not implemented error for Info()
a9d183add chore: set pb.Empty on ssh and secret mounts
d1d3ad8da exec: fix incorrect deps computation for special mounts
509c08c6c Merge pull request #5195 from thaJeztah/0.15_backport_fix_wrong_errdefs
f3ed4635e snapshot/containerd: fix wrong errdefs package import
```

Looks good.  Perform the merge:

```
dancy@base:~/src/wmf/buildkit$ git pull --ff --no-edit upstream v0.15.2
From https://github.com/moby/buildkit
 * tag                   v0.15.2    -> FETCH_HEAD
Removing vendor/github.com/containerd/nydus-snapshotter/pkg/errdefs/errors.go
Merge made by the 'recursive' strategy.
 .golangci.yml                                                        |  4 ++++
 cache/remotecache/v1/utils.go                                        |  5 ++++-
 client/llb/exec.go                                                   |  2 ++
 snapshot/containerd/content.go                                       |  6 +++---
 solver/llbsolver/ops/exec.go                                         |  5 +++++
 vendor/github.com/containerd/nydus-snapshotter/pkg/errdefs/errors.go | 50 --------------------------------------------------
 vendor/modules.txt                                                   |  1 -
 7 files changed, 18 insertions(+), 55 deletions(-)
 delete mode 100644 vendor/github.com/containerd/nydus-snapshotter/pkg/errdefs/errors.go
```

and push to our repo.  Members of the project with `Maintainer` role are allowed to
push directly to the `wmf/v0.15` branch.

```
dancy@base:~/src/wmf/buildkit$ git push origin HEAD
Enumerating objects: 77, done.
Counting objects: 100% (56/56), done.
Delta compression using up to 12 threads
Compressing objects: 100% (31/31), done.
Writing objects: 100% (33/33), 8.62 KiB | 1.72 MiB/s, done.
Total 33 (delta 23), reused 11 (delta 2), pack-reused 0
To gitlab.wikimedia.org:repos/releng/buildkit.git
   6476a407a..e060b151a  HEAD -> wmf/v0.15
```

Tag the merge commit using the `wmf-...` pattern of existing tags.

```
dancy@base:~/src/wmf/buildkit$ git tag wmf-v0.15.2-1
```

Push the tag up to GitLab to start the image build pipeline:

```
dancy@base:~/src/wmf/buildkit$ git push origin wmf-v0.15.2-1
Total 0 (delta 0), reused 0 (delta 0), pack-reused 0
To gitlab.wikimedia.org:repos/releng/buildkit.git
 * [new tag]             wmf-v0.15.2-1 -> wmf-v0.15.2-1
```



After the image build pipeline completes:

Test the new buildkitd image in gitlab-cloud-runner staging.

Edit `staging/staging.tfvars` and update the value
for the `buildkitd_tag` variable.  Commit, push to GitLab and create a
merge request.  When the MR is merged, a pipeline will start which
will pause at `deploy-staging`.  Manually invoke the `deploy-staging`
job.  Wait for the job to complete.  Run a Gitlab CI job that uses
Kokkuri in the staging cluster.  An easy way is to visit
<https://gitlab.wikimedia.org/repos/releng/scap/-/pipelines/new>,
change the branch to `test-staging`, and click `Run pipeline`.

When testing in staging is completed, edit `prod/prod.tfvars` and
update the value for the `buildkitd_tag` variable.  Commit, push to
GitLab and create a merge request.  When the MR is merged, a pipeline
will start which will pause at `push-to-production`.

In #wikimedia-releng on IRC, log a message about the update:

`!log Updating buildkitd to v0.15.2 in gitlab-cloud-runners`

Then Manually invoke the `push-to-production` job.  This will trigger
a new pipeline which will deploy the changes to production.  When that
new pipeline has completed, Run a Gitlab CI job that uses Kokkuri to
verify buildkitd operation.  An easy way is to visit
<https://gitlab.wikimedia.org/repos/releng/scap/-/pipelines/new>, and
click `Run pipeline`.

Update `profile::gitlab::runner::buildkitd_image` in the following files in `operations/puppet`:

* hieradata/cloud.yaml
* hieradata/cloud/eqiad1/devtools/hosts/gitlab-runner-1003.yaml
* hieradata/role/common/gitlab_runner.yaml
* modules/buildkitd/manifests/init.pp

Commit and push the change to Gerrit.  When the change is merged,
Puppet will eventually restart buildkitd on relevant hosts.

