#!/bin/bash

set -eu -o pipefail

cd "$(dirname "$(dirname $0)")"
tmpdir="$(mktemp -d)"

# Ensure that a WMF-built dockerfile frontend is used
sed -e 's,^# syntax=docker/dockerfile-upstream:master,# syntax=docker-registry.wikimedia.org/repos/releng/buildkit/dockerfile-frontend:wmf-v0.20.0-1,' Dockerfile > "$tmpdir/Dockerfile.wmf"
cat wmf/Dockerfile >> "$tmpdir/Dockerfile.wmf"

buildctl build \
  --frontend=dockerfile.v0 \
  --local context=. \
  --local dockerfile="$tmpdir" \
  --opt filename=Dockerfile.wmf \
  "$@"
